public class Account {
    public String accountNumber;
    private String accountType;
    private String givenName;
    private String familyName;
/* Make the constructor*/
    Account(String accountNumber, String accountType, String givenName, String familyName){
        this.accountNumber = accountNumber;
        this.accountType = accountType;
        this.givenName = givenName;
        this.familyName = familyName;
    }
    public String toString(){
        return this.accountNumber + " " + this.accountType + " " + this.givenName + " " +this.familyName;
    }
}
