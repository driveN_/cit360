/*Write a Java class named “DogBean”.
        This class must inherit from the MammalBean class and have the following private properties:
        breed - type String
        name - type String
        The DogBean class must have getter and setter methods for its two properties.
        The DogBean class must have a constructor that accepts and sets values for each of the DogBean and MammalBean properties.
        The DogBean class must have a “toString” method that takes no parameters and returns a String description of the DogBean.
        The description must list out at least each inherited and actual property and the value for each of these. For example, “legCount=4 … name= Fred”.*/
//4a class subclass extends superclass
class DogBean extends MammalBean {
    //Add the two properties
    private String breed;
    private String name;

    //4b Getter and Setter methods for the properties
    public String getBreed() {
        return breed;
    }

    public void setBreed(String newValue) {
        breed = newValue;
    }

    public String getName() {
        return name;
    }

    public void setName(String newValue) {
        name = newValue;
    }

    //4c subclass Dogbean has 2 constructors -- ADD the new properties by inserting it to the subclass
    public DogBean(String theBreed, String theName, int theLegCount, String mammalColor, double mammalHeight) {
        super(theLegCount, mammalColor, mammalHeight);
        breed = theBreed;
        name = theName;
    }

    /* 4d The DogBean class must have a “toString” method that takes no parameters and returns a String description of
    the DogBean. The description must list out at least each inherited and actual property and the value for each of these.
     For example, “legCount=4 … name= Fred”.
     */
    public String toString() {
        return "theLegCount=" + this.getLegCount() + "mammalColor" + this.getColor() + "mammalHeight" + this.getHeight()
                + "theBreed" + this.getBreed() + "theName" + this.getName();
    }
}
