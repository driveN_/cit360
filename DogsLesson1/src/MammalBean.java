/*
Write a Java class named “MammalBean”.
This class must have the following private properties:
legCount - type int
color - type String
height - type double
The MammalBean class must have a constructor that accepts and sets values for each of the three properties.
The MammalBean class must have getter and setter methods for each of the properties.
 */
public class MammalBean { //1
    //2 and 3a, b
       private int legCount;
       private String color;
       private double height;
       //constructor
    public MammalBean(int theLegCount, String mammalColor, double mammalHeight) {
        this.legCount = theLegCount;
        this.color = mammalColor;
        this.height = mammalHeight;
    }
//3c CREATE Getter and Setter Methods
       public int getLegCount() {
           return legCount;
       }
       public int setLegCount(int newValue) {
           legCount = newValue;
           return 0;
       }

    public String getColor() {
        return color;
    }
    public void setColor(String newValue){
           color = newValue;
    }

    public double getHeight() {
           return height;
    }
    public void setHeight(double newValue) {
           height = newValue;
    }
}



