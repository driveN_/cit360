//5a
public class Kennel {
    public DogBean[] buildDogs() {
        DogBean dogCharlie = new DogBean("German Shepperd", "Charlie", 4, "Purple",
                67);
        DogBean dogYoshla = new DogBean("German Shepperd", "Yoshla", 4, "Brown",
                71);
        DogBean dogIvan = new DogBean("German Shepperd", "Ivan", 4, "Black",
                82);
        DogBean dogSarah = new DogBean("Husky", "Sarah", 4, "Pink",
                59);
        DogBean dogGertrude = new DogBean("Labrador", "Gertrude", 4, "Black",
                67);

        DogBean[] dogsArray;
        dogsArray = new DogBean[5];
        dogsArray[0] = dogCharlie;
        dogsArray[1] = dogYoshla;
        dogsArray[2] = dogIvan;
        dogsArray[3] = dogSarah;
        dogsArray[4] = dogGertrude;

        return dogsArray;
    }

    public void displayDogs(DogBean[] houseDogs) {
        for (int i = 0; i < houseDogs.length; i++) {
            System.out.println(houseDogs[i].toString());
        }
    }
    public static void main(String [] args){
        Kennel kennel = new Kennel();
        kennel.displayDogs(kennel.buildDogs());
    }
}
